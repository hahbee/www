attribute vec3 aPosition;
attribute float aSize;

uniform float uFieldScale;
uniform mat4 uViewMatrix;
uniform mat4 uProjectionMatrix;

void main() {
     gl_PointSize = aSize;
     //gl_PointSize = 5.0;
     gl_Position = uProjectionMatrix * uViewMatrix * vec4(aPosition * uFieldScale, 1.0);
}