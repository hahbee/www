-module(www_init).
-author("Richard Diamond <wichard@hahbee.co>").

-export([init/0]).

ensure_started(App) ->
    case application:start(App) of
	ok ->
	    ok;
	{error, {already_started, App}} ->
	    already_started
    end.

init() ->
    case application:start(bcrypt) of
	ok ->
	    _ = ensure_started(mimetypes),
	    ok = mimetypes:load(default, [{<<"vert">>, <<"x-shader/x-vertex">>},
					  {<<"frag">>, <<"x-shader/x-fragment">>},
					  {<<"nmf">>,  <<"application/json">>}]),
	    case boss_env:boss_env() of
		development ->
		    compass_port("."),
		    compass_port("src/scss/member"),
		    hahbee:create_or_update_dev_account();
		_ ->
		    ok
	    end;
	_ ->
	    ok
    end,
    case whereis(riak_db_client) of
	undefined ->
	    {ok, _Pid} = riak_client_sup:start();
	_ ->
	    ok
    end,
    ok.


compass_port(Dir) ->
    spawn(fun() ->
		  process_flag(trap_exit, true),
		  compass_port_loop(undefined, Dir)
	  end),
    ok.

compass_port_loop(undefined, Dir) ->
    compass_port_loop(
      open_port({spawn, "compass watch"}, 
		[stderr_to_stdout, use_stdio, {cd, Dir}, binary]), 
      Dir);
compass_port_loop(Port, Dir) ->
    receive
	{Port, {data, Data}} ->
	    io:fwrite(Data),
	    compass_port_loop(Port, Dir);
	{'EXIT', Port, _} ->
	    compass_port_loop(undefined, Dir)
    end.
