-module(www_outgoing_mail_controller).
-compile(export_all).

%% See http://www.chicagoboss.org/api-mail-controller.html for what should go in here

-define(INVITATION_EMAIL, "invitations@hahbee.com").

test_message(FromAddress, ToAddress, Subject) ->
    Headers = [
        {"Subject", Subject},
        {"To", ToAddress},
        {"From", FromAddress}
    ],
    {ok, FromAddress, ToAddress, Headers, [{address, ToAddress}]}.

invitation(Invitation) ->
    Headers = [{"Subject", "Join the Empire!"},
	       {"To", Invitation:email()},
	       {"From", io_lib:format("~s <~s>", [(Invitation:inviter()):full_name(), ?INVITATION_EMAIL])}],
    {ok, ?INVITATION_EMAIL, Invitation:email(), Headers, [{inviter_name, (Invitation:inviter()):full_name()},
							  {invite_id, Invitation:id()},
							  {email_secret, Invitation:email_secret()}]}.
 
