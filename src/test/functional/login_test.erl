-module(login_test).
-author("Richard Diamond <wichard@hahbee.co>").
-export([start/0, build_continuations/2]).

start() ->
    hahbee:create_or_update_dev_account(),
    Result = boss_web_test:get_request("/login", [],
				       [fun boss_assert:http_ok/1],
				       build_continuations("/", [])),
    hahbee:delete_dev_account(),
    Result.

build_continuations(Redirect, AdditionalContinuations) ->
    NonexistentAccount = 
	fun(Response) ->
		boss_web_test:submit_form("auth", [{"Email", "doesn't matter"},
						   {"Password", "also doesn't matter"}],
					  boss_web_test:cookie_from_response(Response),
					  Response,
					  [fun boss_assert:http_unauthorized/1], [])
	end,
    NoEmail = 
	fun (Response) ->
		boss_web_test:submit_form("auth", [{"Email", absent},
						   {"Password", <<"doesn't matter">>}],
					  boss_web_test:cookie_from_response(Response),
					  Response,
					  [fun boss_assert:http_bad_request/1], [])
	end,
    EmptyEmail = 
	fun(Response) ->
		boss_web_test:submit_form("auth", [{"Email", <<"">>},
						   {"Password", <<"doesn't matter">>}],
					  boss_web_test:cookie_from_response(Response),
					  Response,
					  [fun boss_assert:http_bad_request/1], [])
	end,
    NoPassword = 
	fun(Response) ->
		boss_web_test:submit_form("auth", [{"Email", <<"doesn't matter">>},
						   {"Password", absent}],
					  boss_web_test:cookie_from_response(Response),
					  Response,
					  [fun boss_assert:http_bad_request/1], [])
	end,
    EmptyPassword =
	fun(Response) ->
		boss_web_test:submit_form("auth", [{"Email", <<"doesn't matter">>},
						   {"Password", <<"">>}],
					  boss_web_test:cookie_from_response(Response),
					  Response,
					  [fun boss_assert:http_bad_request/1], [])
	end,
    InvalidPassword =
	fun(Response) ->
		boss_web_test:submit_form("auth", [{"Email", <<"sudo@hahbee.co">>},
						   {"Password", <<"this is not the password">>}],
					  boss_web_test:cookie_from_response(Response),
					  Response,
					  [fun boss_assert:http_unauthorized/1], [])
	end,
    ValidLoginRedirectRequest = 
	fun(Response) ->
		RedirectAssert = 
		    fun(Response) ->
			    boss_assert:location_header(case Redirect of
							    undefined ->
								"/";
							    "" ->
								"/";
							    _ ->
								Redirect
							end, Response)
		    end,
		boss_web_test:submit_form("auth", [{"Email", <<"sudo@hahbee.co">>},
						   {"Password", <<"password">>}],
					  boss_web_test:cookie_from_response(Response),
					  Response, [fun boss_assert:http_redirect/1,
						     RedirectAssert], [])
	end,
    Direct = ["Nonexistent account", NonexistentAccount,
	      "No provided email", NoEmail,
	      "Empty email", EmptyEmail,
	      "No provided password", NoPassword,
	      "Empty password", EmptyPassword,
	      "Invalid password", InvalidPassword,
	      case Redirect of
		  "/" ->
		      "Valid login";
		  _ ->
		      "Valid login redirect request"
	      end, ValidLoginRedirectRequest],
    lists:append(AdditionalContinuations, Direct).
		  
