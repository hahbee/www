-module(visitor_html_test).
-author("Richard Diamond <wichard@hahbee.co>").
-export([start/0]).

start() ->
    boss_web_test:get_request("/member/landing", [],
			      [fun boss_assert:http_redirect/1],
			      ["Follow redirect", fun follow_redirect_to_visitor_landing/1]).

follow_redirect_to_visitor_landing(Res) ->
    boss_web_test:follow_redirect(Res, [fun boss_assert:http_ok/1],
				  []).
