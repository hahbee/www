-module(login_redirect_test).
-author("Richard Diamond <wichard@hahbee.co>").
-export([start/0]).

start() ->
    www_init:init(),
    hahbee:create_or_update_dev_account(),
    RedirectSuffix = "/" ++ binary_to_list(base64:encode(crypto:rand_bytes(64))),
    Result = boss_web_test:get_request("/login" ++ RedirectSuffix,
				       [], [fun boss_assert:http_ok/1],
				       login_test:build_continuations(RedirectSuffix, [])),
    hahbee:delete_dev_account(),
    Result.
