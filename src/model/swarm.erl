-module(swarm, [Id,
		Title           :: string(),
		CreatedOn       :: datetime(),
		LocationTitle   :: string(),
		LocationAddress :: string()]).

-author("Richard Diamond <wichard@hahbee.co>").
-export([id_binary/0, id_integer/0]).
-export([hive_ids/0]).

-define(HIVES_BUCKET, <<"swarm hives">>).

-has({swarm_attendance, many}).
-has({swarm_schedule, many, [{order_by, begin_time}]}).

id_binary() ->
    "swarm-" ++ Id2 = id(),
    list_to_binary(Id2).
id_integer() ->
    "swarm-" ++ Id2 = id(),
    list_to_integer(Id2).

hive_ids() ->
    case get_hives_obj() of
	{ok, Obj} ->
	    riakc_obj:get_values(Obj);
	{error, notfound} ->
	    % i'm not sure what to do on this case.
	    % every swarm is created in a hive
	    % so every swarm should have at least one hive.
	    throw(internal_error)
    end.


%% internal

get_hives_obj() ->
    riakc_pb_socket:get(whereis(riak_db_client), ?HIVES_BUCKET, id_binary()).
