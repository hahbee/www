-module(invitation, [Id,
		     Expires::datetime(),
		     InviterId,
		     Accepted::boolean(),
		     EmailSecret::binary(),
		     Email::string()]).
%-author("Richard Diamond <wichard@hah-bee.com>").
-belongs_to_account(inviter).
-export([]).

new(InviterId, Expires, Email) ->
    invitation:new('id', Expires, InviterId, false, base64:encode(crypto:strong_rand_bytes(8)), Email).
