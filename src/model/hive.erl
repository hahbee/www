-module(hive, [Id,
	       NameStringId :: integer()]).

-export([id_list/0]).
-export([parent_ids/0, children_ids/0]).

-define(HETERARCHY_BUCKET, <<"hive heterarchy">>).
-define(PARENT_KEY, list_to_binary(id_list() ++ "-parents")). 
-define(CHILDREN_KEY, list_to_binary(id_list() ++ "-children")).

id_list() ->
    "hive-" ++ Id2 = id(),
    Id2.

get_parents_obj() ->
    riakc_pb_socket:get(whereis(riak_db_client), ?HETERARCHY_BUCKET, ?PARENT_KEY).
get_children_obj() ->
    riakc_pb_socket:get(whereis(riak_db_client), ?HETERARCHY_BUCKET, ?CHILDREN_KEY).

parent_ids() ->
    case get_parents_obj() of
	{ok, Obj} ->
	    riakc_obj:get_values(Obj);
	{error, notfound} ->
	    []
    end.

children_ids() ->
    case get_children_obj() of
	{ok, Obj} ->
	    riakc_obj:get_values(Obj);
	{error, notfound} ->
	    []
    end.
