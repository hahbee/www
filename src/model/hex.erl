-module(hex, [Id,
	      %ParentId,
	      Created :: datetime(),
	      Taken,
	      BeeId,
	      HoneycombId,
	      Type::integer(),
	      Permissions::binary()]).
-export([id_binary/0, id_integer/0]).
-export([get_content/0, set_content/1, delete_content/0]).
-export([simple_type/0, mime_type/0]).
-export([permissions/0]).
-export([before_delete/0]).

-belongs_to_account(bee).
-belongs_to(honeycomb).

%-belongs_to_hex(parent).
%-has({children, many, [{module, hex}, {foreign_key, parent_id}]}).

-define(BUCKET, <<"hexes">>).

get_content_obj() ->
    riakc_pb_socket:get(whereis(riak_db_client), ?BUCKET, id_binary()).

get_content() ->
    case get_content_obj() of 
	{ok, Obj} ->
	    riakc_obj:get_value(Obj);
	{error, notfound} ->
	    none
    end.
set_content(Content) when is_binary(Content) ->
    Obj = case get_content_obj() of
	      {error, notfound} ->
		  riakc_obj:new(?BUCKET, id_binary(), Content);
	      {ok, Obj2} ->
		  riakc_obj:update_value(Obj2, Content)
	  end,
    ok = riakc_pb_socket:put(whereis(riak_db_client), Obj),
    ok.

delete_content() ->
    case boss_env:boss_env() of
	production ->
	    throw({error, not_allowed});
	_ ->
	    riakc_pb_socket:delete(whereis(riak_db_client), ?BUCKET, id_binary())
    end.

simple_type() ->
    case type() of
	67358791 ->
	    image;
	11057498 ->
	    image;
	57627279 ->
	    image;
	32112763 ->
	    comment;
	102728648 ->
	    video
    end.

mime_type() ->
    case type() of
	67358791 ->
	    <<"image/jpeg">>;
	11057498 ->
	    <<"image/gif">>;
	57627279 ->
	    <<"image/png">>;
	32112763 ->
	    <<"text/plain">>;
	102728648 ->
	    <<"video/webm">>
    end.

id_binary() ->
    "hex-" ++ Id2 = id(),
    list_to_binary(Id2).
id_integer() ->
    "hex-" ++ Id2 = id(),
    list_to_integer(Id2).

permissions() ->
    case Permissions of
	<<>> ->
	    (bee()):default_permissions();
	Permissions2 ->
	    Permissions2
    end.

before_delete() ->
    ok = delete_content(),
    ok.
