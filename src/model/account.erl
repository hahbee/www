-module(account, [Id,
		  CreatedOn::datetime(),
		  Email::string(),
		  Password::binary(),
		  Name::string(),
		  Locale::string(),
		  EarnedInvitations::integer(),
		  DefaultPermissions::binary()]).
%-author("Richard Diamond <wichard@hahbee.co>").
-export([full_name/0]).
-export([before_create/0, before_update/0]).
-export([id_integer/0, raw_default_permissions/0, default_permissions/0]).

-has({invitations, many, [{foreign_key, inviter_id}]}).

-has({swarm, many, [{module, swarm_attendence}, {foreign_key, bee_id}]}).
-has({hex, many, [{foreign_key, bee_id}]}).

%% @doc Get the account's full name.
full_name() ->
    name().
    
before_update() ->
    Modified1 = set([{name, string:strip(Name, both, $ )}]),
    {ok, Modified1}.
before_create() ->
    before_update().

id_integer() ->
    "account-" ++ Id2 = id(),
    list_to_integer(Id2).
    

raw_default_permissions() ->
    DefaultPermissions.
default_permissions() ->
    case raw_default_permissions() of
	<<>> ->
	    <<"TODO">>;
	Permissions ->
	    Permissions
    end.
