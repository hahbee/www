-module(feedback_session, [Id,
			   Start::datetime(),
			   StartReferral::string(),
			   LastActivity::datetime()]).
-table("feedback.sessions").

-has({activity, many, [{module, feedback_activity}, 
		       {order_by, time},
		       {foreign_key, session_id}]}).
-export([before_create/0]).
-export([add_activity/1]).


before_create() ->
    Time = calendar:universal_time(),
    {ok, set([{start, Time},
	      {last_activity, Time}])}.

add_activity(FeedbackActivity) ->
    NewSelf = set(last_activity, calendar:universal_time()),
    NewFBActivity = FeedbackActivity:set(session_id, NewSelf:id()),
    {atomic, _} = boss_db:transaction(fun() ->
					      {ok, _} = NewSelf:save(),
					      {ok, _} = NewFBActivity:save()
				      end),
    ok.
    
