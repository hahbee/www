-module(swarm_attendance, [Id,
			   SwarmId,
			   BeeId,
			   Type      :: integer(),
			   ArrivedAt :: datetime(),
			   LeftAt    :: datetime()]).
-export([type_atom/0]).
-author("Richard Diamond <wichard@hahbee.co>").

-belongs_to_account(bee).
-belongs_to(swarm).

type_atom() ->
    case type() of
	3 ->
	    attended;
	2 ->
	    partially_attended;
	1 ->
	    e_attended;
	0 ->
	    didnt_attend
    end.
