-module(swarm_schedule, [Id,
			 Title::string(),
			 SwarmId,
			 % more compact
			 MidTime::datetime(),
			 DurationSeconds::integer()]).
-author("Richard Diamond <wichard@hahbee.co>").
-export([begin_time/0, end_time/0, set_begin_end_time/2]).
-belongs_to(swarm).

begin_time() ->
    calendar:gregorian_seconds_to_datetime(
      calendar:datetime_to_gregorian_seconds(MidTime) - 
	  round(DurationSeconds / 2.0)).
end_time() ->
    calendar:gregorian_seconds_to_datetime(
      calendar:datetime_to_gregorian_seconds(MidTime) + 
	  round(DurationSeconds / 2.0)).

set_begin_end_time(BeginTime, EndTime) ->
    BeginSeconds = calendar:gregorian_seconds_to_datetime(BeginTime),
    EndSeconds = calendar:gregorian_seconds_to_datetime(EndTime),
    Duration = EndSeconds - BeginSeconds, 
    Mid = calendar:gregorian_seconds_to_datetime(BeginSeconds + round(Duration / 2.0)),
    set([{mid_time, Mid}, {duration_seconds, Duration}]).
