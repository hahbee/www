-module(honeycomb, [Id,
		    Title           :: string(),
		    CreatedOn       :: datetime(),
		    LocationTitle   :: string(),
		    LocationAddress :: string()]).
-author("Richard Diamond <wichard@hahbee.co>").
-export([id_binary/0, id_integer/0]).
-export([hive_ids/0]).

-define(HIVES_BUCKET, <<"honeycomb hives">>).

-has({honeycomb_attendance, many}).
-has({honeycomb_schedule, many, [{order_by, begin_time}]}).

id_binary() ->
    "honeycomb-" ++ Id2 = id(),
    list_to_binary(Id2).
id_integer() ->
    "honeycomb-" ++ Id2 = id(),
    list_to_integer(Id2).

hive_ids() ->
    case get_hives_obj() of
	{ok, Obj} ->
	    riakc_obj:get_values(Obj);
	{error, notfound} ->
	    % i'm not sure what to do on this case.
	    % every honeycomb is created in a hive
	    % so every honeycomb should have at least one hive.
	    throw(interal_error)
    end.


%% internal

get_hives_obj() ->
    riakc_pb_socket:get(whereis(riak_db_client), ?HIVES_BUCKET, id_binary()).
