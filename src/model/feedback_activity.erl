-module(feedback_activity, [SessionId,
			    Time::datatime()]).
-table("feedback.nav").
-belongs_to_feedback_session(session).
-export([before_update/0, before_create/0]).

before_create() ->
    {ok, set(time, calendar:universal_time())}.
before_update() ->
    {error, "Not updatable."}.
