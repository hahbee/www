-module(www_member_controller, [Req, SessionId]).
-author("Richard Diamond <wichard@hahbee.co>").

-export([invite/3, landing/3, logout/3]).

landing('GET', [], Context) ->
    {Account, Context1} = hahbee:get_account(Context),
    {ok, [{account, Account}]}.

invite('GET', [], Account) ->
    ok;
invite('POST', [], Account) ->
    ok.

logout(_, [], Context) ->
    ok = hahbee:deauthorize(Context),
    {redirect, [{controller, visitor}, {action, landing}]}.

