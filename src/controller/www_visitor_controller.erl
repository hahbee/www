-module(www_visitor_controller, [Req, SessionId]).
-author("Richard Diamond <wichard@hahbee.co>").
-export([hello/3]).
-export([landing/3]).
-export([login/3]).
-export([register/3, register/2]).

hello(_, [], Context) ->
    ok.

landing(_, [], Context) ->
    State = hahbee:get_state(Context),
    case hahbee:get_account(State) of
	{undefined, _} ->
	    {redirect, [{controller, visitor}, 
			{action, hello}]};
	{_Account, _} ->
	    {redirect, [{controller, member},
			{action, landing}]}
    end.

login(Method, [], Context) ->
    login(Method, [""], Context);
login('GET', [Referrer], Context) ->
    State = hahbee:get_state(Context),
    {ok, [{referrer, Referrer}, hahbee:create_csrf_token(State)],
    [{"Cache-Control", "no-cache, no-store, must-revalidate"},
     {"Pragma", "no-cache"},
     {"Expires", "0"}]};
login('POST', [Referrer], Context) ->
    State = hahbee:get_state(Context),
    try
	case hahbee:verify_csrf_token(State) of
	    pass ->
		ok;
	    fail ->
		throw({400, "csrf token verification failure"})
	end,
	Email = case Req:post_param("email") of
		    "undefined" ->
			throw({400, "no email"});
		    "" ->
			throw({400, "empty email"});
		    Email1 ->
			Email1
		end,
	PasswordPlainText = case Req:post_param("password") of
				undefined ->
				    throw({400, "no password"});
				"" ->
				    throw({400, "empty password"});
				PW ->
				    PW
			    end,
	Account = case boss_db:find(account, [{email, equals, Email}]) of
		      [A] ->
			  A;
		      _ ->
			  throw({401, io_lib:format("account email, ~p, wasn't found", [Email])})
		  end,
	ActualPasswordHash = Account:password(),
	{ok, HashList} = bcrypt:hashpw(PasswordPlainText, ActualPasswordHash),
	PasswordHash = list_to_binary(HashList),
	if PasswordHash =:= ActualPasswordHash ->
		ok = hahbee:authorize(State, Account),
		{output, <<"">>, [{"Location", 
				   case Referrer of
				       _Loc when _Loc =:= ""  orelse 
						 _Loc =:= "/" orelse 
						 _Loc =:= [] ->
					   "/member";
				       _ ->
					   ensure_absolute(Referrer)
				   end}]};
	   PasswordHash =/= ActualPasswordHash ->
		throw({401, "passwords don't match"})
	end
    catch
	throw:{Status, Reason} when is_integer(Status) ->
	    hahbee:log_if_developing("Failed auth: \"~s\"~n", [Reason]),
	    {Status, hahbee:if_developing(Reason, <<"">>)}
    end.

register('GET', [InvitationId, EmailSecret]) ->
    ok.
register(_, _, _Account) ->
    {redirect, "member landing page"}.

registration('GET', []) ->
    ok;
registration('POST', []) ->
    {output, ""}.


%% internal

ensure_absolute("/" ++ _Rest = Path) ->
    Path;
ensure_absolute(Path) ->
    "/" ++ Path.

