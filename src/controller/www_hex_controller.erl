-module(www_hex_controller, [Req, SessionId]).
-author("Richard Diamond <wichard@hahbee.co>").
-export([summary/3, listen/3]).

content('PUT', [HoneycombId, Type], Context) ->
    ok;
content('GET', [Id], Context) ->
    ok.

summary('GET', [HoneycombId, HiveId], Context) ->
    ok.

listen('GET', [HoneycombId], Context) ->
    ok.

check_permission(AccessingBee, Hex) ->
    try
	case AccessingBee:id() =:= Hex:bee_id() of
	    true ->
		throw(authed); % a bee can always access his/her own hexes.
	    false ->
		ok
	end,
	Permissions = Hex:permissions(),
	case Permissions of
	    % global permission
	    <<3:2, _LocalBits:2>> ->
		throw(authed);
	    <<0:2, 0:2>> ->
		throw(not_authed);
	    _ ->
		ok
	end,
	HoneycombId = Hex:honeycomb_id(),
	
	case Permissions of
	    <<1:2, _LocalBits2:2>> ->
		ok
	end
    catch
	throw:authed ->
	    authed;
	throw:not_authed ->
	    not_authed
    end.
