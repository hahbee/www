-module(www_custom_tags).
-export([format_debug_info/2]).
-export([throw_missing_template_param/2]).

% put custom tags in here, e.g.
%
% reverse(Variables, Options) ->
%     lists:reverse(binary_to_list(proplists:get_value(string, Variables))).
%
% {% reverse string="hello" %} => "olleh"
%
% Variables are the passed-in vars in your template

format_debug_info(Vars, _Options) when length(Vars) > 0 ->
    case boss_env:boss_env() of
	production ->
	    "";
	_ ->
	    lists:foldl(fun({Key, Value}, Acc) when is_atom(Key) andalso (is_list(Value) orelse is_binary(Value)) ->
				Acc ++ io_lib:format(" ~s=\"~s\"", [Key, Value]);
			   ({Key, Value}, Acc) when is_atom(Key) ->
				Acc ++ io_lib:format(" ~s=\"~p\"", [Key, Value])
			end, "<meta", Vars) ++ ">"
    end;
format_debug_info(_, _) ->
    "".

throw_missing_template_param([{param, Param}], _Options) ->
    throw({missing_template_param, Param}).
