-module(www_custom_filters).
-compile(export_all).

% put custom filters in here, e.g.
%
% my_reverse(Value) ->
%     lists:reverse(binary_to_list(Value)).
%
% "foo"|my_reverse   => "oof"

html_bind(Var) when is_binary(Var) ->
    io_lib:format("{{ ~s }}", [Var]).
