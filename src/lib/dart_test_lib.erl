-module(dart_test_lib).
-author("Richard Diamond <wichard@hahbee.co>").
-export([cache_headers/2]).

cache_headers(MTime, ETag) ->
    [fun(Req) ->
	     boss_assert:content_type_header("application/dart", Req)
     end,
     fun(Req) ->
	     boss_assert:header("Last-Modified", httpd_util:rfc1123_date(MTime), Req)
     end,
     fun(Req) ->
	     boss_assert:header("ETag", httpd_util:integer_to_hexlist(ETag), Req)
     end].

