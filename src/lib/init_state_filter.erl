-module(init_state_filter).
-author("Richard Diamond <wichard@hahbee.co>").
-export([before_filter/2]).

before_filter(_, Context) ->
    State = hahbee:create_state(
	      proplists:get_value(session_id, Context),
	      proplists:get_value(request, Context)),
    {ok, lists:append([{state, State}], Context)}.
