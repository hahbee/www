-module(hahbee).
-author("Richard Diamond <wichard@hah-bee.com>").

-export([create_state/2,
	 set_state/2,
	 get_state/1,
	 get_account/1,
	 get_account_throw/1,
	 get_request/1,
	 get_session/1]).
-export([update_activity/1, 
	 get_feedback_session/1, 
	 authorize/3, authorize/2, 
	 authorized/1,
	 deauthorize/1]).
-export([extra_params/1, 
	 create_or_update_dev_account/0,
	 delete_dev_account/0]).
-export([create_csrf_token/1, 
	 verify_csrf_token/1]).
-export([log_if_developing/2,
	 if_developing/2]).

-define(SESSION_AUTH_KEY, authorization_key).
-define(SESSION_FEEDBACK_KEY, feedback_key).
-define(SESSION_CSRF_KEY, csrf_key).
-record(state, {session_id,
		request,
		account}).

create_state(SessionId, Req) ->
    #state{session_id = SessionId,
	   request = Req}.

get_state([{state, #state{} = State} | _] = _Context) ->
    State;
get_state(Context) ->
    #state{} = proplists:get_value(state, Context).
set_state(Context, #state{} = State) ->
    lists:keyreplace(state, 1, Context, {state, State}).
get_account(#state{session_id = Session, account = undefined} = State) ->
    case boss_session:get_session_data(Session, ?SESSION_AUTH_KEY) of
	undefined ->
	    {undefined, State#state{account = none}};
	Account ->
	    {Account, State#state{account = Account}}
    end;
get_account(#state{account = none} = State) ->
    {undefined, State};
get_account(#state{account = Account} = State) ->
    {Account, State};
get_account(Context) when is_list(Context) ->
    {Account, State} = get_account(get_state(Context)),
    {Account, set_state(Context, State)}.
get_account_throw(Context) when is_list(Context) ->
    case get_account(Context) of
	{undefined, _Context} ->
	    throw('unauthorized! internal_error');
	{_Account, _Context} = Ret ->
	    Ret
    end.
get_request(#state{request = R}) ->
    R;
get_request(Context) when is_list(Context) ->
    get_request(get_state(Context)).
get_session(#state{session_id = Session} = _State) ->
    Session;
get_session(Context) when is_list(Context) ->
    get_session(get_state(Context)).


update_activity(#state{session_id = SessionId} = State) ->
    {ok, FeedbackSession} = get_feedback_session(State),
    FeedbackActivity = feedback_activity:new(FeedbackSession:id(), undefined),
    ok = FeedbackSession:add_activity(FeedbackActivity).

get_feedback_session(#state{session_id = SessionId, request = Req} = _State) ->
    case boss_session:get_session_data(SessionId, ?SESSION_FEEDBACK_KEY) of 
	{error, _Reason} ->
	    failed;
	[] ->
	    {ok, (feedback_session:new('id', undefined, Req:header(referer), undefined)):save()};
	[FeedbackSession] ->
	    {ok, FeedbackSession}
    end.

authorized(#state{session_id = SessionId} = State) ->
    case boss_session:get_session_data(SessionId, ?SESSION_AUTH_KEY) of
	{error, _Reason} = Error ->
	    failed;
	undefined ->
	    failed;
	Account ->
	    {ok, Account, State#state{account = Account}}
    end.

authorize(#state{} = _State, "", "") ->
    failed;
authorize(#state{}, "", _PasswordPlainText) ->
    failed;
authorize(#state{}, _Email, "") ->
    failed;
authorize(#state{session_id = SessionId} = _State, Email, PasswordPlainText) ->
    case boss_db:find(account, [{email, eq, Email}]) of
	[] ->
	    failed;
	[Account] ->
	    PassSalt = Account:password(),
	    {ok, SubmittedPassHash} = bcrypt:hashpw(PasswordPlainText, PassSalt),
	    case list_to_binary(SubmittedPassHash) =:= PassSalt of
		false ->
		    failed;
		true ->
		    ok = boss_session:set_session_data(SessionId, ?SESSION_AUTH_KEY, Account),
		    {ok, Account}
	    end
    end.
authorize(#state{session_id = Session} = _State, Account) ->
    ok = boss_session:set_session_data(Session, ?SESSION_AUTH_KEY, Account),
    ok.

deauthorize(ContextOrState) ->
    Session = get_session(ContextOrState),
    boss_session:remove_session_data(Session, ?SESSION_AUTH_KEY).

new_invitation_email(Account, Email) ->
    Invitation = invitation:new(Account:id(), 
				edate:shift(calendar:universal_time(), 5, day),
				Email),
    boss_mail:send_template(www, invitation, [Invitation], fun({ok, _Receipt}) ->
								   ok;
							      ({error, Reason}) ->
								   %% TODO
								   ok
							   end).


extra_params(#state{session_id = SessionId, request = Req} = _State) ->
    if_developing([{debug, true}], []).

%% @doc Create a dummy account for use while developing. Does not require an invitation.
create_or_update_dev_account() ->
    Email = "sudo@hahbee.co",
    {Id, CreatedOn} = case boss_db:find(account, [{email, equals, Email}]) of
	     [] ->
		 {'id', erlang:localtime()};
	     [SudoAccount] ->
		 {SudoAccount:id(), SudoAccount:created_on()}
	 end,
    {ok, Salt} = bcrypt:gen_salt(),
    {ok, Pass} = bcrypt:hashpw(<<"password">>, Salt),
    Account = account:new(Id, CreatedOn, Email, list_to_binary(Pass), "Erlang Developer", "en-US", 99, <<>>), 
    Account:save().
delete_dev_account() ->
    Email = "sudo@hahbee.co",
    case boss_db:find_first(account, [{email, eq, Email}]) of
	undefined ->
	    ok;
	Account ->
	    boss_db:delete(Account:id())
    end,
    ok.

%% @doc 
create_csrf_token(#state{session_id = Session} = _State) ->
    Token = crypto:rand_bytes(8),
    boss_session:set_session_data(Session, ?SESSION_CSRF_KEY, Token),
    {csrf_token, base64:encode(Token)}.
verify_csrf_token(#state{session_id = Session, request = Req} = _State) ->
    try
	Token = case boss_session:get_session_data(Session, ?SESSION_CSRF_KEY) of
		    undefined ->
			throw(fail);
		    T when is_binary(T) ->
			T
		end,
	PostToken = case Req:post_param("csrf_token") of
			undefined ->
			    throw(fail);
			T2 ->
			    base64:decode(T2)
		    end,
	if PostToken =:= Token ->
		pass;
	   PostToken =/= Token ->
		throw(fail)
	end
    catch
	throw:fail ->
	    fail
    end.

log_if_developing(Format, Args) ->
    case boss_env:boss_env() of
	development ->
	    io:fwrite(Format, Args);
	_ ->
	    ok
    end.
if_developing(IfTesting, IfNotTesting) ->
    case boss_env:boss_env() of
	production ->
	    IfNotTesting;
	_ ->
	    IfTesting
    end.


