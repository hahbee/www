-module(auth_filter).
-author("Richard Diamond <wichard@hahbee.co>").
-export([before_filter/2]).

%-include_lib("hahbee.hrl").

before_filter(_Config, RequestContext) ->
    State = hahbee:get_state(RequestContext),
    {Authed, RequestContext1, State1} = case hahbee:authorized(State) of
				    failed ->
					{not_authed, RequestContext, State};
				    {ok, Account, S} ->
					{{authed, Account}, 
					 lists:keyreplace(state, 1, RequestContext, {state, S}),
					 S}
				end,
    
    Controller = proplists:get_value(controller_module, RequestContext1),
    Action = proplists:get_value(action, RequestContext1),
    case auth_for_controller_action(Authed, 
				    Controller,
				    Action,
				    hahbee:get_request(State1),
				    RequestContext1) of
	ok ->
	    {ok, RequestContext1};
	ControllerResult ->
	    ControllerResult
    end.

auth_for_controller_action({authed, _},
			   www_member_controller,
			   _,
			   _Request,
			   _) ->
    ok;
auth_for_controller_action(not_authed,
			   www_member_controller,
			   "logout",
			   _Request,
			   _) ->
    {redirect, [{controller, visitor}, {action, login}]};
auth_for_controller_action(not_authed,
			   www_member_controller,
			   _,
			   Request,
			   _) ->
    {redirect, [{controller, visitor}, {action, login},
		{referrer, Request:path()}]};
auth_for_controller_action(not_authed,
			   www_data_controller,
			   _,
			   _Request,
			   _) ->
    {401, <<>>};
auth_for_controller_action({authed, _},
			   www_data_controller,
			   _Action,
			   _Request,
			   _Context) ->
    ok;
auth_for_controller_action({authed, _}, 
			   www_visitor_controller, 
			   "login",
			   Request,
			   Context) ->
    Referrer = case proplists:get_value(tokens, Context) of
		   [R] when is_list(R) andalso length(R) > 0 ->
		       R;
		   _ ->
		       "/member"
	       end,
    {redirect, Referrer};
auth_for_controller_action(not_authed,
			   www_visitor_controller,
			   "login",
			   _Request,
			   _) ->
    ok;
auth_for_controller_action(_,
			   www_visitor_controller,
			   "hello",
			   _Request,
			   _) ->
    ok;
auth_for_controller_action(_,
			   www_visitor_controller,
			   "landing",
			   _Request,
			   _) ->
    ok;
auth_for_controller_action(_,
			   www_dart_script_controller,
			   _,
			   _,
			   _) ->
    ok.



