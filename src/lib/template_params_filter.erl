-module(template_params_filter).
-author("Richard Diamond <wichard@hahbee.co>").
-export([middle_filter/3]).

middle_filter({render, Params, Headers}, _, Context) ->
    {render, Params ++ hahbee:extra_params(hahbee:get_state(Context)), Headers};
middle_filter(Other, _, _) ->
    Other.
