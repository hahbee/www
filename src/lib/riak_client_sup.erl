-module(riak_client_sup).
-author("Richard Diamond <wichard@hahbee.co>").
-behaviour(supervisor).

-export([start/0, init/1]).

start() ->
    supervisor:start_link(?MODULE, []).

init([]) ->
    {ok, {{one_for_one, 60, 60},
	  [{riak_client,
	    {riak_client, start_link, []},
	    permanent, 5, worker, [riak_client]}]}}.
