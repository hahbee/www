-module(debug_filter).
-author("Richard Diamond <wichard@hahbee.co>").
-export([middle_filter/3]).

-include("hahbee.hrl").

middle_filter(ActionResult, _FilterConfig, Context) -> 
    State = case proplists:get_value(state, Context) of
		undefined ->
		    hahbee:create_state(
		      proplists:get_value(session_id, Context),
		      proplists:get_value(request, Context));
		S ->
		    S
	    end,
    add_params(ActionResult, hahbee:extra_params(State)).
    

add_params({ok, Params}, ExtraParams) when is_list(ExtraParams) ->
    {ok, lists:append(ExtraParams, Params)};
add_params(ActionResult, _ExtraParams) when is_list(_ExtraParams) ->
    ActionResult.
