-module(riak_client).
-author("Richard Diamond <wichard@hahbee.co>").
-behaviour(gen_server).

-export([start_link/0, init/1]).

start_link() ->
    gen_server:start_link(?MODULE, [], []).

init([]) ->
    {ok, Pid} = riakc_pb_socket:start_link("127.0.0.1", 10017),
    case whereis(riak_db_client) of
	undefined ->
	    register(riak_db_client, Pid);
	_ ->
	    unregister(riak_db_client),
	    register(riak_db_client, Pid)
    end,
    {ok, {Pid}}.

